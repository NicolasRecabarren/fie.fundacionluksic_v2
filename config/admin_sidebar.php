<?php
$config = [
    'Sidebar' => [
        /*0 => [
            'props' => [ # Propiedades
                'name' => 'Default', # nombre
                'ico' => false, # ícono (se obtiene desde font-awesome)
                'target' => false, # id de data-target (solo modificar si tiene submenú)
                'url' => false, # modificar solo si redirigirá al clickear en la pestaña principal del submenú
                'current' => [ # current se usa para agregar la clase "active" a la pestaña escogida
                    'controller' => false, # controller actual
                    'plugin' => false # plugin actual, si no tiene dejar en false
                ]
            ],
            'options-list' => false, # si no tiene submenú dejar en false
            'options-list' => [ 
                0 => [
                    'name' => '', # nombre a desplegar en submenú
                    'url' => '', # url de submenú
                    'current' => [ # current se usa para la clase active
                        'action' => '', # vista de submenú
                        'controller' => '', # controller de submenú
                        'plugin' => false # plugin de submenú
                    ]
                ],
                ...
            ]
        ],*/
        #0 => [
        #    'props' => [
        #        'name' => 'Dashboard',
        #        'ico' => 'fa-dashboard',
        #        'target' => 'sidebar-dashboard',
        #        'url' => '/admin/dashboard',
        #        'current' => [
        #            'controller' => 'Users',
        #            'plugin' => 'Users'
        #        ]
        #    ],
        #    'options-list' => false
        #],
        1 => [
            'props' => [
                'name' => 'Usuarios',
                'ico' => 'fa-users',
                'target' => 'sidebar-users',
                'url' => false,
                'current' => [
                    'controller' => 'Users',
                    'plugin' => 'Users'
                ]
            ],
            'options-list' => [
                0 => [
                    'name' => 'Listado',
                    'url' => '/admin/usuarios',
                    'current' => [
                        'action' => 'admin_index',
                        'controller' => 'Users',
                        'plugin' => 'Users'
                    ]
                ],
                1 => [
                    'name' => 'Crear',
                    'url' => '/admin/usuarios/crear',
                    'current' => [
                        'action' => 'admin_add',
                        'controller' => 'Users',
                        'plugin' => 'Users'
                    ]
                ]
            ]
        ],
        2 => [
            'props' => [
                'name' => 'Temporadas',
                'ico' => 'fa-hourglass-o',
                'target' => 'sidebar-seasons',
                'url' => false,
                'current' => [
                    'controller' => 'Seasons',
                ]
            ],
            'options-list' => [
                0 => [
                    'name' => 'Listado',
                    'url' => '/admin/temporadas',
                    'current' => [
                        'action' => 'admin_index',
                        'controller' => 'Seasons',
                        'plugin' => false
                    ]
                ],
                1 => [
                    'name' => 'Crear',
                    'url' => '/admin/temporadas/crear',
                    'current' => [
                        'action' => 'admin_add',
                        'controller' => 'Seasons',
                        'plugin' => false
                    ]
                ]
            ]
        ],
        3 => [
            'props' => [
                'name' => 'Configuración correos',
                'ico' => 'fa-envelope-o',
                'target' => 'sidebar-conf-email',
                'url' => '/admin/config_emails',
                'current' => [
                    'controller' => 'ConfigEmails'
                ]
            ],
            'options-list' => false
        ]
    ]
];