$(document).ready(function(){
    loadLogsList();
    $('#AdminDashboardShow,#AdminDashboardOrder,#AdminDashboardAutor').change(function(){
        loadLogsList();
    });
    $('#AdminDashboardActions').change(function(){
        $('#AdminDashboardForm').submit();
    });
    function loadLogsList(){
        var dataJson = $('#AdminDashboardForm').serialize();
        $.ajax({
            url: '/admin/dashboard/obtener-logs',
            type: 'POST',
            data: dataJson,
            //dataType: 'json',
            success: function(data){
                $('.logs-users-list').html(data);
            }
        });
    }
});