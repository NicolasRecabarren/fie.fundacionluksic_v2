<?php
namespace Users\Controller;

use Users\Controller\AppController;
use Cake\Mailer\Email;
use Cake\Utility\Hash;

/**
 * Users Controller
 *
 * @property \Users\Model\Table\UsersTable $Users
 */
class UsersController extends AppController{

    public function initialize(){
        parent::initialize();
        $this->loadComponent('Paginator');
        $this->Auth->allow([
            'admin_requestNewPassword',
            'admin_changePassword',
            'admin_add'
        ]);
    }
    
    /**
     * Login method
     *
     * @return \Cake\Network\Response|null
     */
    public function admin_login(){
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error('Nombre de usuario o contraseña incorrectos.');
        }
    }
    
    /**
     * Logout method
     *
     * @return \Cake\Network\Response|null Redirects to index.
     */
    public function admin_logout(){
        $this->Flash->success('Te haz desconectado.');
        return $this->redirect($this->Auth->logout());
    }
    
    public function register(){
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('El usuario ha sido guardado.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('El usuario no pudo ser guardado. Por favor, intente nuevamente.'));
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }
    
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function admin_index(){
        $this->paginate = [
            'contain' => ['Userinfos']
        ];
        $users = $this->paginate($this->Users);

        $this->set('roles', $this->Users->_roles);
        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }
    
    /**
     * admin_dashboard method
     * Mostramos el dashboard del usuario administrador, con los logs correspondientes.
     *
     * @return \Cake\Network\Response|null
     */
    public function admin_dashboard(){
        $logsTable = parent::getTableInstance('Logs');
        $userLogsTable = parent::getTableInstance('Users.UserLogs');
        
        if($this->request->is('post')){
            if(empty($this->request->data['actions'])){
                $this->Flash->error(__('Escoja una opción válida.'));
            } else{
                # Si seleccionó marcar como leídos (seleccionados) o eliminar (seleccionados)
                if(isset($this->request->data['chk']) && in_array($this->request->data['actions'],[1,3])){
                    $selected = Hash::extract($this->request->data['chk'],'{n}.id');
                    if(empty($selected)){
                        $this->Flash->error(__('Debe escojer por lo menos un registro.'));
                    } else {
                        if($this->request->data['actions'] == 1){
                            $query = $userLogsTable->find('all',[
                                'conditions' => ['UserLogs.id IN' => $selected]
                            ])->toArray();
                            foreach($query as $row){
                                $row->status = 1;
                                $userLogsTable->save($row);
                            }
                            $this->Flash->success(__('Registros guardados como leídos.'));
                        }else{
                            $userLogsTable->deleteAll(['id' => $selected]);
                        }
                    }
                } else {
                    
                }
            }
        }
        
        
        
        
        $logs = $logsTable->find('all');
        
        
        
        $cantNewUsers = $userLogsTable->find('all',['conditions' => ['action' => 'create']])->count();
        
        $users = $this->Users->find('list',[
            'keyField' => 'id',
            'valueField' => 'username'
        ])->toArray();
        
        $this->set(compact('logs','cantNewUsers','users'));
        $this->set('_serialize', ['logs','userLogs']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null){
        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function admin_add(){
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data,[
                'associated' => [
                    'Userinfos'
                ]
            ]);
            if($this->Users->hasBehavior('Log')){
                $this->Users->setUserToLog($this->Auth->user('id'));
            }
            if ($this->Users->save($user)) {
                $this->Flash->success(__('El usuario ha sido guardado.'));
                return $this->redirect(['action' => 'index', 'admin' => true]);
            } else {
                $this->Flash->error(__('El usuario no pudo ser guardado. Por favor, intente nuevamente.'));
            }
        }
        $this->set('roles',$this->Users->_roles);
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function admin_edit($id = null){
        $user = $this->Users->get($id, [
            'contain' => ['Userinfos']
        ]);
        unset($user->password);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if($this->Users->hasBehavior('Log')){
                $this->Users->setUserToLog($this->Auth->user('id'));
            }
            if ($this->Users->save($user)) {
                $this->Flash->success(__('El usuario ha sido guardado.'));

                return $this->redirect(['action' => 'index', 'admin' => true]);
            } else {
                $this->Flash->error(__('El usuario no pudo ser guardado. Por favor, intente nuevamente.'));
                unset($user->password);
                unset($user->confirm_password);
            }
        }
        $this->set('roles',$this->Users->_roles);
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function admin_delete($id = null){
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if($this->Users->hasBehavior('Log')){
            $this->Users->setUserToLog($this->Auth->user('id'));
        }
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('El usuario ha sido eliminado.'));
        } else {
            $this->Flash->error(__('El usuario no pudo ser eliminado. Por favor, intente nuevamente.'));
        }

        return $this->redirect(['action' => 'index', 'admin' => true]);
    }
    
    /**
     * admin_changeStatus
     *
     * @param int $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function admin_changeStatus($id = null){
        $user = $this->Users->get($id);
        $user->status = $user->status ? 0 : 1;
        $this->Users->save($user);
        $this->Flash->success(__('Usuario actualizado.'));
        return $this->redirect(['action' => 'index', 'admin' => true]);
    }
    
    /**
     * admin_requestNewPassword function
     *
     * Busca si existe el mail ingresado para enviarle un correo con el link para renovar la contraseña
     *
     * @return void
     */
    public function admin_requestNewPassword(){
        if($this->request->is('post')){
            $query = $this->Users->find('all',[
                'conditions' => [
                    'Users.email' => $this->request->data['email']
                ]
            ])->first();
            if(empty($query)){
                $this->Flash->error('El correo electrónico no está registrado.');
            } else {
                $this->sendRequestPasswordEmail($query);
            }
        }
    }
    
    /** Custom Functions **/
    
    /**
     * admin_changePassword
     *
     * @param string $token
     * @return a redirect to admin_login
     */
    public function admin_changePassword($token){
        $user = $this->Users->findByToken($token)->first();
        unset($user->password);
        
        # validamos que el token exista
        if(empty($user)){
            $this->Flash->error(__('Token inválido.'));
            return $this->redirect(['action' => 'login', 'admin' => true]);
        
        # validamos que el token no haya expirado (tiene una duración de 10 minutos)
        } elseif($user->token_expired->format('Y-m-d H:i') < date('Y-m-d H:i')){
            $this->Flash->error(__('El token ha expirado.'));
            return $this->redirect(['action' => 'login', 'admin' => true]);
            
        } else {
            if($this->request->is('post') || $this->request->is('put')){
                $this->Users->patchEntity($user,[
                    'password' => $this->request->data['password'],
                    'confirm_password' => $this->request->data['confirm_password']],[
                    'validate' => 'updatePassword'
                ]);
                if($this->Users->save($user)){
                    $this->Flash->success(__('La contraseña ha sido cambiada con éxito.'));
                    return $this->redirect(['action' => 'login', 'admin' => true]);
                }
                $this->Flash->error(__('Ha ocurrido un error al guardar la contraseña.'));
            }
        }
        $this->set('user',$user);
    }
    
    /**
     * sendRequestPasswordEmail Function
     * 
     * Enviamos un email con el link de la función que permitirá al usuario
     * recuperar su contraseña.
     * 
     * @param object $user
     * @return null
     */
    function sendRequestPasswordEmail($user){
        $email = parent::getEmailInstance();
        $email->transport('gmail')
              ->from('fie.fundacionluksic@gmail.com')
              ->subject('Nueva solicitud de recuperación de contraseña')
              ->template('Users.admin_request_password')
              ->emailFormat('html')
              ->to($user->email)
              ->viewVars([
                'username' => $user->username,
                'token' => $this->generateToken($user)
              ]);
        try{
            $email->send();
            $this->Flash->success(__('Se ha enviado un mensaje al correo electrónico.'));
        }catch(Exception $exc){
            $this->Flash->error(__('Ha ocurrido un error al enviar el email.'));
        }
    }
    
    /**
     * generateToken Function
     *
     * genera un token que estará habilitado por los próximos 10 minutos para cambiar
     * la contraseña de un usuario.
     *
     * @param object $user
     * @return string $token
     */
    function generateToken($user){
        $cadena = 'abcdefghijklmnopqrstuvwxyz1234567890%';
        $token = substr(str_shuffle($cadena),0,14);
        $user->token = $token;
        $user->token_expired = date('Y-m-d H:i',strtotime('+10 minute'));
        $this->Users->save($user);
        return $token;
    }
    
    /** Logs Functions **/
    
    
    /**
     * admin_getLogList method
     *
     * Obtiene una lista de los registros de la tabla user_logs
     *
     * @return void;
     */
    public function admin_getLogList(){
        $conditions = array();
        $order['created'] = 'DESC';
        if($this->request->is('post')){
            if(!empty($this->request->data['show'])){
                if($this->request->data['show'] == 1)
                    $conditions['action'] = 'create';
                elseif($this->request->data['show'] == 2)
                    $conditions['action'] = 'update';
                else
                    $conditions['action'] = 'delete';
            }
            if(!empty($this->request->data['order']))
                $order['created'] = ($this->request->data['order'] == 0) ? 'DESC' : 'ASC';
            if(!empty($this->request->data['autor']))
                $conditions['user_id'] = $this->request->data['autor'];
        }
        $userLogsTable = parent::getTableInstance('Users.UserLogs');
        $userLogs = $userLogsTable->find('all',[
            'order' => $order,
            'conditions' => $conditions
        ]);
        $users = $this->Users->find('list',[
            'keyField' => 'id',
            'valueField' => 'username'
        ])->toArray();
        $this->set(compact('userLogs','users'));
    }
    
    /**
     * admin_showLog method
     *
     * Carga contenido dinámico en el modal de los logs en dashboard
     * 
     * @param int $id UserLog id.
     * @return void;
     */
    public function admin_showLog($id = null){
        $userLogsTable = parent::getTableInstance('Users.UserLogs');
        
        $conditions['id'] = $id;
        $log = $userLogsTable->find('all',[
            'conditions' => $conditions
        ])->first();
        $log->json_data = json_decode($log->json_data);
        
        $users = $this->Users->find('list',[
            'keyField' => 'id',
            'valueField' => 'username'
        ])->toArray();
        $this->set(compact('log','users','id'));
        $this->set('_serialize', ['log']);
    }
    
    /**
     * admin_deleteLog method
     *
     * @param int $id UsersLog id.
     * @return \Cake\Network\Response|null Redirects to dashboard.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function admin_deleteLog($id = null){
        $logsTable = parent::getTableInstance('Users.UserLogs');
        $log = $logsTable->get($id);
        if($logsTable->delete($log)){
            $this->Flash->success(__('Registro eliminado.'));
        } else {
            $this->Flash->error(__('No se pudo eliminar el registro.'));
        }
        return $this->redirect(['action' => 'dashboard', 'admin' => true]);
    }
}