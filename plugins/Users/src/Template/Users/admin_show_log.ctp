<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel"><?=__('Registro ID:');?> <?=$id;?></h4>
</div>
<div class="modal-body">
    <table class="table">
        <tbody>
            <tr>
                <td style="font-weight: bold;"><?=__('ID Usuario:');?></td>
                <td><?=$log->associated_id;?></td>
            </tr>
            <tr>
                <td style="font-weight: bold;"><?=__('Nombre Usuario:');?></td>
                <td><?=$log->json_data->username;?></td>
            </tr>
            <tr>
                <td style="font-weight: bold;"><?=__('Email:');?></td>
                <td><?=$log->json_data->email;?></td>
            </tr>
            <tr>
                <td style="font-weight: bold;"><?=__('Autor');?></td>
                <td><?=isset($users[$log->user_id]) ? $users[$log->user_id] : 'N/A';?></td>
            </tr>
            <tr>
                <td style="font-weight: bold;"><?=__('Status');?></td>
                <td>
                    <? if($log->action == 'create'){ ?>
                        <span class="label label-success"><?=__('Creado');?></span>
                    <? } elseif($log->action == 'update') { ?>
                        <span class="label label-warning"><?=__('Modificado');?></span>
                    <? } else { ?>
                        <span class="label label-danger"><?=__('Eliminado');?></span>
                    <? } ?>
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold;"><?=__('Fecha');?></td>
                <td><?=$log->created->format('Y-m-d H:i:s');?></td>
            </tr>
        </tbody>
    </table>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?=__('Cerrar');?></button>
    <?=$this->Html->link(__('Eliminar Registro'),
            ['plugin' => 'Users', 'controller' => 'Users', 'action' => 'deleteLog', 'admin' => true, $id],
            ['class' => 'btn btn-primary', 'rel-id' => $id]);?>
    <!--<button type="button" class="btn btn-primary delete-log" rel-id="<?#=$id;?>"><?#=__('Eliminar Registro');?></button>-->
</div>