<?php $this->assign('title', 'FIE | Editar Usuario'); ?>
<section class="content-header">
    <h1 class="page-header"><?=__('Editar Usuario');?></h1>
    <ol class="breadcrumb">
        <li>
            <i class="fa fa-dashboard"></i>
            <?=$this->Html->link(__('Dashboard'),['action' => 'dashboard', 'admin' => true]);?>
        </li>
        <li>
            <i class="fa fa-users"></i>
            <?=$this->Html->link(__('Usuarios'),['action' => 'index', 'admin' => true]);?>
        </li>
        <li class="active">
            <i class="fa fa-edit"></i> <?=__('Editar Usuario');?>
        </li>
    </ol>
</section>
<section class="content">
    <div class="row" style="margin-bottom: 20px;">
        <div class="col-lg-8 col-md-8">
            <?=$this->Flash->render();?>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"></h3>
                </div>
                <?=$this->Form->create($user,[
                    'novalidate' => true,
                    'templates' => [
                        'inputContainer' => '
                            <div class="form-group {{errorClass}}">
                                {{content}}
                                {{errorMessage}}
                            </div>'
                    ]
                ]);?>
                <div class="box-body">
                    <?php
                        echo $this->Form->hidden('status',['value' => 1]);
                    
                        # Nombre de Usuario
                        echo $this->Form->input('username',[
                                'label' => [
                                    'class' => 'control-label',
                                    'text' => __('Nombre de Usuario')
                                ],
                                'class' => 'form-control',
                                'templateVars' => [
                                    'errorClass' => ($this->Form->isFieldError('username') ? 'has-error' : ''),
                                    'errorMessage' => $this->Form->error('username')
                                ],
                                'error' => false
                        ]);
                        
                        # Contraseña
                        echo $this->Form->input('password',[
                                'label' => [
                                    'class' => 'control-label',
                                    'text' => __('Contraseña')
                                ],
                                'class' => 'form-control',
                                'templateVars' => [
                                    'errorClass' => ($this->Form->isFieldError('password') ? 'has-error' : ''),
                                    'errorMessage' => $this->Form->error('password')
                                ],
                                'error' => false
                        ]);
                        
                        # Confirmar Contraseña
                        echo $this->Form->input('confirm_password',[
                                'label' => [
                                    'class' => 'control-label',
                                    'text' => __('Confirmar contraseña')
                                ],
                                'class' => 'form-control',
                                'templateVars' => [
                                    'errorClass' => ($this->Form->isFieldError('password') ? 'has-error' : ''),
                                    'errorMessage' => ''
                                ],
                                'type' => 'password',
                                'error' => false
                        ]);
                        
                        # Email
                        echo $this->Form->input('email',[
                                'label' => [
                                    'class' => 'control-label',
                                    'text' => __('Email')
                                ],
                                'templateVars' => [
                                    'errorClass' => ($this->Form->isFieldError('email') ? 'has-error' : ''),
                                    'errorMessage' => $this->Form->error('email')
                                ],
                                'class' => 'form-control',
                                'error' => false
                        ]);
                        
                        # Rut
                        echo $this->Form->input('userinfo.rut',[
                                'label' => [
                                    'class' => 'control-label',
                                    'text' => __('Rut')
                                ],
                                'templateVars' => [
                                    'errorClass' => ($this->Form->isFieldError('userinfo.rut') ? 'has-error' : ''),
                                    'errorMessage' => $this->Form->error('userinfo.rut')
                                ],
                                'class' => 'form-control',
                                'error' => false
                        ]);
                        
                        # Nombre
                        echo $this->Form->input('userinfo.name',[
                                'label' => [
                                    'class' => 'control-label',
                                    'text' => __('Nombre')
                                ],
                                'class' => 'form-control',
                                'templateVars' => [
                                    'errorClass' => ($this->Form->isFieldError('userinfo.name') ? 'has-error' : ''),
                                    'errorMessage' => $this->Form->error('userinfo.name')
                                ],
                                'error' => false
                        ]);
                        
                        # Apellidos
                        echo $this->Form->input('userinfo.lastname',[
                                'label' => [
                                    'class' => 'control-label',
                                    'text' => __('Apellidos')
                                ],
                                'templateVars' => [
                                    'errorClass' => ($this->Form->isFieldError('userinfo.lastname') ? 'has-error' : ''),
                                    'errorMessage' => $this->Form->error('userinfo.lastname')
                                ],
                                'class' => 'form-control',
                                'error' => false
                        ]);
                        
                        # Dirección
                        echo $this->Form->input('userinfo.address',[
                                'label' => [
                                    'class' => 'control-label',
                                    'text' => __('Dirección')
                                ],
                                'templateVars' => [
                                    'errorClass' => ($this->Form->isFieldError('userinfo.address') ? 'has-error' : ''),
                                    'errorMessage' => $this->Form->error('userinfo.address')
                                ],
                                'class' => 'form-control',
                                'type' => 'text',
                                'error' => false
                        ]);
                        
                        # Teléfono Fijo
                        echo $this->Form->input('userinfo.tel_phone',[
                                'label' => [
                                    'class' => 'control-label',
                                    'text' => __('Teléfono fijo')
                                ],
                                'templateVars' => [
                                    'errorClass' => ($this->Form->isFieldError('userinfo.tel_phone') ? 'has-error' : ''),
                                    'errorMessage' => $this->Form->error('userinfo.tel_phone')
                                ],
                                'class' => 'form-control',
                                'type' => 'text',
                                'error' => false
                        ]);
                        
                        # Teléfono Celular
                        echo $this->Form->input('userinfo.tel_cellphone',[
                                'label' => [
                                    'class' => 'control-label',
                                    'text' => __('Teléfono celular')
                                ],
                                'templateVars' => [
                                    'errorClass' => ($this->Form->isFieldError('userinfo.tel_cellphone') ? 'has-error' : ''),
                                    'errorMessage' => $this->Form->error('userinfo.tel_cellphone')
                                ],
                                'class' => 'form-control',
                                'error' => false
                        ]);
                        
                        # Rol
                        echo $this->Form->input('role',[
                                'empty' => 'Seleccione',
                                'label' => false,
                                'options' => $roles,
                                'templates' => [
                                    'inputContainer' => '
                                        <div class="form-group {{errorClass}}">
                                            <label>{{label}}</label>
                                            {{content}}
                                            {{errorMessage}}
                                        </div>'
                                ],
                                'templateVars' => [
                                    'label' => 'Rol',
                                    'errorClass' => ($this->Form->isFieldError('role') ? 'has-error' : ''),
                                    'errorMessage' => $this->Form->error('role')
                                ],
                                'class' => 'form-control',
                                'error' => false
                        ]);?>
                    <?=$this->Form->submit(__('Guardar'),['class' => 'btn btn-success', 'div' => false]);?>
                </div>
                <?= $this->Form->end();?>
            </div>
        </div>
    </div>
</section>