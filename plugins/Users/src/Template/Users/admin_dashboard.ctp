<?php $this->assign('title', 'FIE | Dashboard'); ?>
<?php echo $this->Html->script('Users.admin_get_log_list'); ?>
<section class="content-header">
    <h1>Dashboard</h1>
    <ol class="breadcrumb">
        <li>
            <i class="fa fa-dashboard"></i>
            <?=$this->Html->link('Home',['action' => 'dashboard', 'admin' => true],['title' => 'Ir a','escape' => false]);?>
        </li>
        <li class="active">
            Dashboard
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><?=$cantNewUsers;?></h3>
                    <p><?=__('Nuevos usuarios');?></p>
                </div>
                <div class="icon"><i class="ion ion-person-add"></i></div>
                <?=$this->Html->link('Más información <i class="fa fa-arrow-circle-right"></i>','',['class' => 'small-box-footer', 'escape' => false]);?>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>22</h3>
                    <p><?=__('Nuevas postulaciones');?></p>
                </div>
                <div class="icon"><i class="ion ion-ios-paper"></i></div>
                <?=$this->Html->link('Más información <i class="fa fa-arrow-circle-right"></i>','',['class' => 'small-box-footer', 'escape' => false]);?>
            </div>
        </div>
    </div>
    <div class="box box-primary">
        <div class="box-header">
            <i class="ion ion-clipboard"></i>
            <h3 class="box-title"><?=__('Registros de Usuarios');?></h3>
        </div>
        <?=$this->Form->create('Filter',['novalidate' => true, 'id' => 'AdminDashboardForm']);?>
        <div class="box-body">
            <?=$this->Flash->render();?>
                <div class="form-group col-lg-4">
                    <label><?=__('Mostrar:');?></label>
                    <?=$this->Form->select('show',[
                        __('Todos'),
                        __('Sólo creaciones'),
                        __('Sólo modificaciones'),
                        __('Sólo eliminaciones')
                    ],[
                        'class' => 'form-control',
                        'id' => 'AdminDashboardShow'
                    ]);?>
                </div>
                <div class="form-group col-lg-4">
                    <label><?=__('Ordenar:');?></label>
                    <?=$this->Form->select('order',[
                        __('Más recientes primero'),
                        __('Más antiguos primero')
                    ],[
                        'class' => 'form-control',
                        'id' => 'AdminDashboardOrder'
                    ]);?>
                </div>
                <div class="form-group col-lg-4">
                    <label><?=__('Autor:');?></label>
                    <?=$this->Form->select('autor',$users,[
                        'class' => 'form-control',
                        'empty' => 'Seleccione...',
                        'id' => 'AdminDashboardAutor'
                    ]);?>
                </div>
            
            <div class="logs-users-list" style="height: 400px; max-height: 400px;border: 2px solid #E6E7E8; display: flex;"></div>
            <div class="form-group col-lg-3">
                <label><?=__('Acciones para seleccionados:');?></label>
                <?=$this->Form->select('actions',[
                    1 => __('Marcar como leídos (Seleccionados)'),
                    2 => __('Marcar como leídos (Todos)'),
                    3 => __('Eliminar (Seleccionados)'),
                    4 => __('Eliminar (Todos)')
                ],[
                    'empty' => __('Seleccione...'),
                    'class' => 'form-control',
                    'id' => 'AdminDashboardActions'
                ]);?>
            </div>
        </div>
        <?=$this->Form->end();?>
    </div>
</section>