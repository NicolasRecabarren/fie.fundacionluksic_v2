<style type="text/css">
    .dark-li {
        background: #ECE9E9 none repeat scroll 0 0 !important;
    }
    .todo-list li {
        border-left: none;
    }
</style>
<ul class="todo-list col-lg-1 col-xs-1"><?php
    foreach($userLogs as $key => $log): ?>
        <li style="height: 40px;">
            <?=$this->Form->input('chk.'.$key.'.id',[
                'templates' => [
                    'inputContainer' => '
                        <div class="checkbox" style="margin: 0;">
                            <label>{{content}}</label>
                        </div>'
                ],
                'type' => 'checkbox',
                'value' => $log->id,
                'hiddenField' => false,
                'label' => false
            ]);?>
        </li><?php
    endforeach; ?>
</ul>
<ul class="todo-list col-lg-11 col-xs-11" id="accordion"><?
    foreach($userLogs as $key => $log):
        $totalSeconds = strtotime('now') - strtotime($log->created->format('Y-m-d H:i:s'));
        $hours = floor($totalSeconds/3600);
        
        if($hours == 0){
            $minutes = floor(($totalSeconds/60)%60);
            $text = __('Hace ').$minutes.__(' minutos');
        } elseif($hours > 23) {
            $days = floor($hours/24);
            $text = __('Hace ').$days.__(' días');
        }else{
            $text = __('Hace ').$hours.__(' horas');
        } ?>
        <li style="cursor: pointer;" rel-id="<?=$log->id;?>" data-toggle="modal" data-target="#logInfoModal">
            <div class="li-header">
                <span class="text"><?=$log->message;?></span>
                <small class="label label-info"><i class="fa fa-clock-o"></i> <?=$text;?></small>
            </div>
        </li><?
    endforeach; ?>
</ul>
<div class="modal fade" id="logInfoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content"></div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#accordion li').on('click',function(){
            $.ajax({
                url: '/admin/dashboard/mostrar-log/'+$(this).attr('rel-id'),
                success: function(data){
                    $('.modal-content').html(data);
                }
            });
        });
        $('#accordion li').hover(function(){
            var assoc = $(this).attr('rel-id');
            if($(this).hasClass('dark-li')){
                $(this).removeClass('dark-li');
                $('#chk-'+assoc).parent().parent().parent().removeClass('dark-li');
            }else{
                $(this).addClass('dark-li');
                $('#chk-'+assoc).parent().parent().parent().addClass('dark-li');
            }
        });
        $('.checkbox').hover(function(){
            var assoc = $(this).find('input[type="checkbox"]').val();
            if($(this).parent().hasClass('dark-li')){
                $(this).parent().removeClass('dark-li');
                $('#accordion li[rel-id="'+assoc+'"]').removeClass('dark-li');
            }else{
                $(this).parent().addClass('dark-li');
                $('#accordion li[rel-id="'+assoc+'"]').addClass('dark-li');
            }
        });
        $('.checkbox').on('click',function(){
            var assoc = $(this).find('input[type="checkbox"]').val();
            if($(this).parent().hasClass('dark-li')){
                $(this).parent().removeClass('dark-li');
                $('#accordion li[rel-id="'+assoc+'"]').removeClass('dark-li');
            }else{
                $(this).parent().addClass('dark-li');
                $('#accordion li[rel-id="'+assoc+'"]').addClass('dark-li');
            }
        });
    });
</script>