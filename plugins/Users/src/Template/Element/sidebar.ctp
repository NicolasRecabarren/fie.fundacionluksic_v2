<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <?=$this->Html->image('user2-160x160.jpg',['class' => 'img-circle', 'alt' => 'Profile Image', 'style' => 'height: 45px;']);?>
            </div>
            <div class="pull-left info">
                <p>Nicolas Recabarren</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
        <ul class="sidebar-menu">
            <li class="header"><?=__('Administración');?></li><?php
            foreach($sidebar as $option):
                #obtenemos el elemento principal activo
                $active = '';
                if($this->request->params['controller'] == $option['props']['current']['controller'])
                    if(isset($option['props']['current']['plugin']) && $option['props']['current']['plugin']){
                        if ($option['props']['current']['plugin'] == $this->request->params['plugin'])
                            $active = 'active';
                    } else {
                        $active = 'active';
                    }
                
                #obtenemos el data-target
                $data_target = '';
                if($option['props']['target'] && !empty($option['props']['target']))
                    $data_target = $option['props']['target'];
                    
                #obtenemos url de pestaña principal
                $main_url = 'javascript:;';
                if($option['props']['url'])
                    $main_url = $option['props']['url'];
                        
                $hasSubmenu = ($option['options-list']) ? true : false;
                $hasIco = ($option['props']['ico']) ? true : false;
            
                if($hasSubmenu):?>
                    <li class="<?=$active;?> treeview">
                        <a href="<?=$main_url;?>">
                            <i class="fa <?=$hasIco ? $option['props']['ico'] : '';?>"></i> <span><?=$option['props']['name'];?></span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu"><?php
                            foreach($option['options-list'] as $option_list):
                                # Obtenemos el elemento activo
                                $active = '';
                                if($this->request->params['controller'] == $option_list['current']['controller'])
                                    if($this->request->params['action'] == $option_list['current']['action'])
                                        if(!$option_list['current']['plugin'])
                                            $active = 'active';
                                        else
                                            if($this->request->params['plugin'] == $option_list['current']['plugin'])
                                                $active = 'active';?>
                            
                                <li class="<?=$active;?>">
                                    <?=$this->Html->link('<i class="fa fa-circle-o"></i> '.$option_list['name'],$option_list['url'],['escape' => false]);?>
                                </li><?php
                            endforeach; ?>
                        </ul>
                    </li>
                <?php else: ?>
                    <li><a href="<?=$main_url;?>"><i class="fa <?=$hasIco ? $option['props']['ico'] : '';?>"></i> <span><?=$option['props']['name'];?></span></a></li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
        <ul class="sidebar-menu">
            <li class="header"><?=__('Front');?></li>
        </ul>
    </section>
</aside>