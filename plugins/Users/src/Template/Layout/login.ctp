<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>AdminLTE 2 | Log in</title>
        
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        
        <?=$this->Html->css('bootstrap.min.css'); # Bootstrap 3.3.6 ?>
        <?=$this->Html->css('font-awesome.min.css'); # Font Awesome ?>
        <?=$this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css'); # Ionicons ?>
        <?=$this->Html->css('/dist/css/AdminLTE.min.css'); # Theme style ?>
        <?=$this->Html->css('/plugins/iCheck/square/blue.css'); # iCheck ?>
        <?=$this->Html->css('bootstrap-datepicker.css'); # Bootstrap Datepicker ?>
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <?=$this->fetch('meta') ?>
        <?=$this->fetch('css') ?>
        <?=$this->fetch('script') ?>
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <a href="../../index2.html"><b>Admin</b>LTE</a>
            </div>
            <?=$this->fetch('content');?>
        </div>
        <?=$this->Html->script('/plugins/jQuery/jquery-2.2.3.min.js'); # jQuery 2.2.3 ?>
        <?=$this->Html->script('bootstrap.min.js'); # Bootstrap 3.3.6 ?>
        <?=$this->Html->script('bootstrap-datepicker.min.js'); # Bootstrap Datepicker?>
        <?=$this->Html->script('admin_oxus'); # Custom functions ?>
    </body>
</html>