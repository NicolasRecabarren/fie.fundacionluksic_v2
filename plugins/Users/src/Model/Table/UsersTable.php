<?php
namespace Users\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \Cake\ORM\Association\BelongsTo $UserInfos
 *
 * @method \Users\Model\Entity\User get($primaryKey, $options = [])
 * @method \Users\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \Users\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \Users\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Users\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Users\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \Users\Model\Entity\User findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table{
    
    public $_roles = [
        0 => 'Administrador',
        1 => 'Usuario'
    ];
    
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config){
        parent::initialize($config);

        $this->table('users');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->addBehavior('Log',[
            'plugin' => 'Users',
            'useTable' => 'UserLogs',
            'messageCreate' => __('Se ha creado un nuevo usuario.'),
            'messageUpdate' => __('Se ha modificado un usuario.'),
            'messageDelete' => __('Se ha eliminado un usuario')
        ]);
        $this->belongsTo('Userinfos', [
            'foreignKey' => 'userinfo_id',
            'className' => 'Users.Userinfos'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator){
        
        $validator
            ->requirePresence('username', 'true')
            ->notEmpty('username','Debe escribir un nombre de usuario.')
            ->add('username',[
                'unique' => [
                    'rule' => 'validateUnique',
                    'provider' => 'table',
                    'message' => 'Nombre de usuario ya registrado.'
                ]
            ]);

        $validator
            ->requirePresence('password','true')
            ->notEmpty('password','Debe escribir una contraseña.')
            ->add('password', [
                'compare' => [
                    'rule' => ['compareWith', 'confirm_password'],
                    'message' => 'Las contraseñas no coinciden.'
                ]
            ]);

        $validator
            ->requirePresence('email', 'true')
            ->notEmpty('email','Debe escribir su correo electrónico.')
            ->add('email',[
                'valid-email' => [
                    'rule' => 'email',
                    'message' => 'Formato de email inválido.'
                ],
                'unique' => [
                    'rule' => 'validateUnique',
                    'provider' => 'table',
                    'message' => 'Email ya registrado.'
                ]
            ]);

        $validator
            ->requirePresence('role', 'true')
            ->notEmpty('role', 'Debe escoger un rol de usuario');

        $validator
            ->allowEmpty('token_password');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules){
        $rules->add($rules->isUnique(['username']));
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }
    
    public function validationUpdatePassword(Validator $validator ){
        $validator
            ->add('password', [
                'length' => [
                    'rule' => ['minLength', 6],
                    'message' => 'La contraseña debe contener por lo menos 6 caracteres.',
                ],
                'match' => [
                    'rule' => ['compareWith','confirm_password'],
                    'message' => 'Las contraseñas no coinciden.',
                ]
            ])
            ->notEmpty('password');
 
        return $validator;
    }
}