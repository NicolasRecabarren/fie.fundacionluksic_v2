<?php
namespace Users\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserInfos Model
 *
 * @property \Cake\ORM\Association\HasMany $Users
 *
 * @method \Users\Model\Entity\UserInfo get($primaryKey, $options = [])
 * @method \Users\Model\Entity\UserInfo newEntity($data = null, array $options = [])
 * @method \Users\Model\Entity\UserInfo[] newEntities(array $data, array $options = [])
 * @method \Users\Model\Entity\UserInfo|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Users\Model\Entity\UserInfo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Users\Model\Entity\UserInfo[] patchEntities($entities, array $data, array $options = [])
 * @method \Users\Model\Entity\UserInfo findOrCreate($search, callable $callback = null)
 */
class UserinfosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('userinfos');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->hasMany('Users', [
            'foreignKey' => 'userinfo_id',
            'className' => 'Users.Users'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator){

        $validator
            ->requirePresence('rut', 'true')
            ->notEmpty('rut','Debe ingresar el rut.')
            ->add('rut',[
                'unique' => [
                    'rule' => 'validateUnique',
                    'provider' => 'table',
                    'message' => 'Rut ya registrado.'
                ]
            ]);

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name', 'Campo Obligatorio.');

        $validator
            ->requirePresence('lastname', 'create')
            ->notEmpty('lastname', 'Campo Obligatorio.');

        $validator
            ->requirePresence('address', 'create')
            ->notEmpty('address', 'Campo Obligatorio.');

        $validator
            ->requirePresence('tel_phone', 'create')
            ->notEmpty('tel_phone', 'Campo Obligatorio.');

        $validator
            ->requirePresence('tel_cellphone', 'create')
            ->notEmpty('tel_cellphone', 'Campo Obligatorio.');

        return $validator;
    }
}
