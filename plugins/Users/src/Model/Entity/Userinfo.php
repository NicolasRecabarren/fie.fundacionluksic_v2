<?php
namespace Users\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserInfo Entity
 *
 * @property int $id
 * @property string $rut
 * @property string $name
 * @property string $lastname
 * @property string $address
 * @property string $tel_phone
 * @property string $tel_cellphone
 *
 * @property \Users\Model\Entity\User[] $users
 */
class Userinfo extends Entity{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
    
    protected function _getFullName(){
        return $this->_properties['name'] . '  ' .$this->_properties['lastname'];
    }
}
