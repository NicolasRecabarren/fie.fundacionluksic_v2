$(document).ready(function(){
    //Datepicker
    $('.datepicker').datepicker({
        format: "yyyy-mm-dd",
        startDate: "1910-01-01",
        viewMode: "years",
    });
});