<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * EmailConfigs Model
 *
 * @method \App\Model\Entity\EmailConfig get($primaryKey, $options = [])
 * @method \App\Model\Entity\EmailConfig newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\EmailConfig[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\EmailConfig|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\EmailConfig patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\EmailConfig[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\EmailConfig findOrCreate($search, callable $callback = null)
 */
class EmailConfigsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('email_configs');
        $this->displayField('id');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator){
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('email', 'true')
            ->notEmpty('email','Campo obligatorio.')
            ->add('email',[
                'valid-email' => [
                    'rule' => 'email',
                    'message' => 'Formato de email inválido.'
                ]
            ]);

        $validator
            ->requirePresence('password', 'true')
            ->notEmpty('password','Campo obligatorio.');

        $validator
            ->requirePresence('protocol', 'true')
            ->notEmpty('protocol','Campo obligatorio.');

        $validator
            ->requirePresence('from', 'true')
            ->notEmpty('from','Campo obligatorio.');

        $validator
            ->requirePresence('host', 'true')
            ->notEmpty('host','Campo obligatorio.');

        $validator
            ->requirePresence('port', 'true')
            ->notEmpty('port','Campo obligatorio.');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }
}
