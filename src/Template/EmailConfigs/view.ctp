<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Email Config'), ['action' => 'edit', $emailConfig->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Email Config'), ['action' => 'delete', $emailConfig->id], ['confirm' => __('Are you sure you want to delete # {0}?', $emailConfig->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Email Configs'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Email Config'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="emailConfigs view large-9 medium-8 columns content">
    <h3><?= h($emailConfig->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Email') ?></th>
            <td><?= h($emailConfig->email) ?></td>
        </tr>
        <tr>
            <th><?= __('Password') ?></th>
            <td><?= h($emailConfig->password) ?></td>
        </tr>
        <tr>
            <th><?= __('Protocol') ?></th>
            <td><?= h($emailConfig->protocol) ?></td>
        </tr>
        <tr>
            <th><?= __('From Name') ?></th>
            <td><?= h($emailConfig->from_name) ?></td>
        </tr>
        <tr>
            <th><?= __('Host') ?></th>
            <td><?= h($emailConfig->host) ?></td>
        </tr>
        <tr>
            <th><?= __('Port') ?></th>
            <td><?= h($emailConfig->port) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($emailConfig->id) ?></td>
        </tr>
    </table>
</div>
