<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Email Config'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="emailConfigs index large-9 medium-8 columns content">
    <h3><?= __('Email Configs') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('email') ?></th>
                <th><?= $this->Paginator->sort('password') ?></th>
                <th><?= $this->Paginator->sort('protocol') ?></th>
                <th><?= $this->Paginator->sort('from_name') ?></th>
                <th><?= $this->Paginator->sort('host') ?></th>
                <th><?= $this->Paginator->sort('port') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($emailConfigs as $emailConfig): ?>
            <tr>
                <td><?= $this->Number->format($emailConfig->id) ?></td>
                <td><?= h($emailConfig->email) ?></td>
                <td><?= h($emailConfig->password) ?></td>
                <td><?= h($emailConfig->protocol) ?></td>
                <td><?= h($emailConfig->from_name) ?></td>
                <td><?= h($emailConfig->host) ?></td>
                <td><?= h($emailConfig->port) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $emailConfig->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $emailConfig->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $emailConfig->id], ['confirm' => __('Are you sure you want to delete # {0}?', $emailConfig->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
