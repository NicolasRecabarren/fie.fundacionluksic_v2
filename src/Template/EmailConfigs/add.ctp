<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Email Configs'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="emailConfigs form large-9 medium-8 columns content">
    <?= $this->Form->create($emailConfig) ?>
    <fieldset>
        <legend><?= __('Add Email Config') ?></legend>
        <?php
            echo $this->Form->input('email');
            echo $this->Form->input('password');
            echo $this->Form->input('protocol');
            echo $this->Form->input('from_name');
            echo $this->Form->input('host');
            echo $this->Form->input('port');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
